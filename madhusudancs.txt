Madhusudan Cheelangi
~~~~~~~~~~~~~~~~~~~~

Education
~~~~~~~~~
2011–(expected 2013) Master Of Science, Computer Science, University of California, Irvine, GPA: 3.90/4.00.
2005–2009 Bachelor Of Engineering, Information Science And Engineering, Visvesvaraya Technological University, B.M.S. College Of Engineering, Bangalore, First class with distinction (80.23%).
2003–2005 Pre University College, M.E.S College Of Arts, Science And Commerce, Bangalore, First class with distinction (86%).

Technical skills
~~~~~~~~~~~~~~~~
Languages And APIs - Proficient: Python: Django, Google Appengine; Ruby: Ruby on Rails; C/C++ Web: Javascript, CSS; SQL, PHP
Languages And APIs - Aware: Python: Sage, SciPy, NumPy, Matplotlib;
Tools: LaTeX, Beamer, reSt, XML, XSL/XSLT, MySQL, SQLite3, RPC programming, Translator Programming in GNU/Hurd, Drupal 
System Administration: GNU/Linux; Shell scripting (BASH) Server Apache server, Mercurial servers
SCMs: git, hg, CVS, SVN

Bachelor thesis
~~~~~~~~~~~~~~~
Title: Astute Song Hunter: Playback File Retrieval From Media Servers Based On Humming Patterns

Advisor: Prof. R.Ashok Kumar

Along with 2 other team mates, I designed and implemented a search engine that takes in the hum of the tune as a query, and efficiently searches and retrieves the (closest) corresponding songs from the media server. In this paper we have implemented a technique for searching a song based on its acoustic properties. I was particularly responsible for designing and implementing one of the two algorithms used for comparisons, the MaxMin approach and the for the searching technique which matched the processed query against the database on the server side. Paper, source code and more details at: http://code.google.com/p/astute-song-hunter/

Projects
~~~~~~~~

Academic - UI Design for U.S. Presidential Elections simulation game
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Along with the 3 other team mates, I created the wireframes and designs using HotGloo wireframe system for implementing an online multiplayer real-time game that simulates the U.S. Presidential Elections. I was particularly responsible for designing the home screen, the controls and the dashboards.

Google Summer of Code 2009 - Implementation of Task-based GCI-like Work Flow for Melange
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Implemented a system, where administrators can create and mange tasks, students can claim these tasks and submit their work, which can be reviewed by the mentors which helps to run the Google Code-in (GCI) contest. This code is already in use and Google Code-in 2010 and 2011 contests were run the on this code http://code.google.com/gci. This contest was previously called Google Highly Open Participation(GHOP). The project was implemented in Python, Django and Google Appengine. Source: http://bitbucket.org/madhusudancs/melange-mq

Google Summer of Code 2008 - GNU/Linux Compatible /proc Filesystem for GNU Hurd Operating System
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Implemented a sophisticated procfs pseudo-filesystem for "the Hurd" Operating System. The project thus made the GNU/Linux process management tools like ps, top, vmstat, skill, nice, pgrep, pstree, etc., run out of the box without porting the them to Hurd. Source: http://github.com/madhusudancs/procfs

Paper - Optimal Streaming Protocol for Video-on-Demand Using Client’s Residual Bandwidth
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Along with the 3 other team mates, I designed a protocol through which videos can be streamed through well connected network using cooperative chaining. The protocol avoided wastage of bandwidth by implementing dual level chaining at the client and the proxy server levels. I was particularly responsible for designing the
protocol and the chaining mechanisms.

Internship at Vee Technologies - Dynamic Form Generation Application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Along with 3 other team mates, I implemented a web application, written in PHP, to generate forms on the fly based on the definitions given by the admins. We used MySQL, XML and related technologies like XSL and XSLT for data management. I was particularly responsible for the modules that stored, retrieved and manipulated meta-data of the dynamic forms in XML format. I was also responsible for the module that rendered the form given the meta-data.

Academic - Co-Sequential Processing Of Files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Implemented a "Co-Sequential Files Processing System" in C++ where the bank transactions will be processed and posted on a day-to-day basis.


Experience
~~~~~~~~~~

October 2011–Current - Advanced Ruby on Rails Developer, Office of Information Technology, University of California, Irvine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
I work on the academic personnels project called "Recruit". Recruit is a University of California wide online system for recruiting faculty to the respective universities. I am responsible for implementing new features on the backend. The project is written in Ruby on Rails.


January, 2009–Current -  Core Developer, Melange
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
I am a core developer (committer) on the Melange project. Melange is used by Google’s Open Source Programs Office to run its Google Summer of CodeTM and Google Code-inTM programs. I work on Melange along with a globally distributed team. I work on all the aspects of the project from Python, Django and Google Appengine code in the backend to HTML, CSS and Javascript code used in the frontend. I have over 1000 commits to date. http://code.google.com/p/soc/

July, 2009–July, 2011 Reasearch Assistant, Free and Open Source software in Science and Engineering Education(FOSSEE), Indian Institute of Technology, Bombay.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Worked as a content generator, instructor, programmer for Python for scientific computing packages and also the programmer for websites that fossee maintained like http://scipy.in etc. Also maintained the Mercurial server hosted at http://hg.fossee.in and served as a part-time sys-admin for maintaining fossee.in servers to fill in the shortage of sys-admin resources. As part of the work I delivered courses at various educational institutes in India. Have delivered talks/lectures in ten workshops and two conferences till date.

Google Summer of Code 2011 - Mentor, Google Code-In with new architecture, Melange project
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Mentored an University student during the summer to implement the Google Code-in (previously GHOP) module based on the Melange v2 architecture. 

Google Summer of Code 2010 - Mentor, Social features for Melange, Melange project
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Mentored an University student during the summer to implement the features like user profiles, calendar and documents sharing, etc. for Melange to make it more social.

Reference
~~~~~~~~~

Carol Smith
Program Manager, Google Open Source Programs Office
carols@google.com

Prof. Prabhu Ramachandran
Assistant Professor, Department of Aerospace Engineering, Indian Institute of Technology - Bombay
prabhu@aero.iitb.ac.in


Contact
~~~~~~~
Address: 3801 Parkview Lane Apt 23A – 92612 Irvine, CA
Phone: +1-949-864-9635 
Email: madhusudancs@gmail.com (preferred), mcheelan@uci.edu
Profile: https://github.com/madhusudancs, https://plus.google.com/113141282929084508748/posts, https://www.ohloh.net/accounts/madhusudancs

